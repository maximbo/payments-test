import functools

from project.db import db


def rollback_after(func):
    @functools.wraps(func)
    async def wrapped(*args, **kwargs):
        async with db.transaction() as transaction:
            await func(*args, **kwargs)
            transaction.raise_rollback()
    return wrapped
