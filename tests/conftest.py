import os

import pytest
from sqlalchemy_utils import create_database, drop_database, database_exists
from alembic import command as alembic_command
from alembic.config import Config as AlembicConfig
from starlette.testclient import TestClient
from asgi_lifespan import LifespanManager
from httpx import AsyncClient

os.environ['TESTING'] = 'True'
from project.config import TEST_DATABASE_URL
from project.app import app
from project.services.auth import AuthService


@pytest.fixture(scope="session", autouse=True)
def create_test_database():
    url = str(TEST_DATABASE_URL)
    assert not database_exists(url), \
        'Test database already exists. Aborting tests.'

    # Create test database
    create_database(url)

    # Run Alembic migrations
    base_dir = os.path.dirname(os.path.dirname(__file__))
    alembic_cfg = AlembicConfig(os.path.join(base_dir, "alembic.ini"))
    alembic_command.upgrade(alembic_cfg, "head")

    # Run tests
    yield

    # Drop test database
    drop_database(url)


@pytest.fixture
async def test_app():
    async with LifespanManager(app):
        yield app


@pytest.fixture
async def aclient(test_app):
    async with AsyncClient(app=test_app, base_url="http://app.io") as client:
        yield client
