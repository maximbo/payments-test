import pytest

from project.services.auth import AuthService

from tests.utils import rollback_after


async def create_test_customer(username="test", password="test"):
    service = AuthService()
    auth_user, customer = await service.create_with_customer(
        username="test",
        password="test",
    )
    token = service.create_access_token(auth_user)

    return auth_user, customer, token


@pytest.mark.asyncio
@rollback_after
async def test_account_get_list(aclient):
    user, _, token = await create_test_customer()

    resp = await aclient.get(
        "/api/accounts",
        headers=token.header,
    )
    assert resp.status_code == 200

    data = resp.json()
    assert len(data) == 3


@pytest.mark.asyncio
@rollback_after
async def test_account_account_create(aclient):
    user, _, token = await create_test_customer()

    resp = await aclient.get(
        "/api/accounts",
        headers=token.header,
    )
    assert resp.status_code == 200
    assert len(resp.json()) == 3
