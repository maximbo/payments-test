import pytest

from project.services.auth import AuthService

from tests.utils import rollback_after


@pytest.mark.asyncio
@rollback_after
async def test_login(aclient):
    service = AuthService()
    await service.create_with_customer(
        username="test",
        password="test",
    )

    url = "/api/auth/token"
    resp = await aclient.post(url, data={
        "username": "test",
        "password": "test",
    })

    assert resp.status_code == 200
    assert resp.json()["access_token"]
    assert resp.json()["token_type"] == "bearer"


@pytest.mark.asyncio
@rollback_after
async def test_invalid_password_login(aclient):
    service = AuthService()
    await service.create_with_customer(
        username="test",
        password="test")

    url = "/api/auth/token"
    resp = await aclient.post(url, data={
        "username": "test",
        "password": "invalid",
    })

    assert resp.status_code == 401


@pytest.mark.asyncio
@rollback_after
async def test_invalid_username_login(aclient):
    service = AuthService()
    await service.create_with_customer(
        username="test",
        password="test")

    url = "/api/auth/token"
    resp = await aclient.post(url, data={
        "username": "where",
        "password": "test",
    })

    assert resp.status_code == 401
