import pytest

from project.db import models
from tests.utils import rollback_after


@pytest.mark.asyncio
@rollback_after
async def test_registration(aclient):
    url = "/api/auth/register"

    resp = await aclient.post(
        url,
        json={
            "username": "test",
            "password": "test",
        }
    )

    assert resp.status_code == 201
    data = resp.json()
    assert data["token"]["access_token"]
    assert data["token"]["token_type"] == "bearer"

    assert data["username"] == "test"

    assert data["customer"]["first_name"] == ''
    assert data["customer"]["middle_name"] == ''
    assert data["customer"]["last_name"] == ''


@pytest.mark.asyncio
@rollback_after
async def test_full_registration(aclient):
    url = "/api/auth/register"

    resp = await aclient.post(
        url,
        json={
            "username": "test",
            "password": "test",
            "first_name": "Snow",
            "last_name": "Snow",
        }
    )

    assert resp.status_code == 201
    data = resp.json()
    assert data["token"]["access_token"]
    assert data["token"]["token_type"] == "bearer"

    assert data["username"] == "test"

    assert data["customer"]["first_name"] == 'Snow'
    assert data["customer"]["middle_name"] == ''
    assert data["customer"]["last_name"] == 'Snow'


@pytest.mark.asyncio
@rollback_after
async def test_empty_username_registration(aclient):
    url = "/api/auth/register"

    resp = await aclient.post(
        url,
        json={
            "username": "",
            "password": "test",
            "first_name": "Snow",
            "last_name": "Snow",
        }
    )

    assert resp.status_code == 422
    assert resp.json()["detail"][0]["loc"] == ["body", "username"]


@pytest.mark.asyncio
@rollback_after
async def test_empty_password_registration(aclient):
    url = "/api/auth/register"

    resp = await aclient.post(
        url,
        json={
            "username": "test",
            "password": "",
            "first_name": "Snow",
            "last_name": "Snow",
        }
    )

    assert resp.status_code == 422
    assert resp.json()["detail"][0]["loc"] == ["body", "password"]


@pytest.mark.asyncio
@rollback_after
async def test_taken_username_registration(aclient):
    url = "/api/auth/register"

    await models.AuthUser.create(username="test")

    resp = await aclient.post(
        url,
        json={
            "username": "test",
            "password": "test",
            "first_name": "Snow",
            "last_name": "Snow",
        }
    )
    assert resp.status_code == 409
