.PHONY: server test lint shell


server:
	poetry run uvicorn project.app:app --reload

test:
	poetry run pytest tests -x

lint:
	poetry run mypy project

shell:
	poetry run python
