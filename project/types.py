from uuid import UUID
from enum import Enum
from decimal import Decimal


Money = Decimal


class Currency(Enum):
    RUB = "RUB"
    USD = "USD"
    EUR = "EUR"
