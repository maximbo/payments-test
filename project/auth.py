from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer

from project.services.auth import AuthService
from project.db import models


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="auth/token")


async def get_current_user(
    token=Depends(oauth2_scheme),
) -> models.AuthUser:
    goaway = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )

    auth_service = AuthService()
    user = await auth_service.get_by_token(token)
    if user is None:
        raise goaway

    return user
