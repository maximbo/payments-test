import datetime
from enum import Enum
from typing import List, Optional, cast

from fastapi import APIRouter, Depends, HTTPException
from pydantic import BaseModel
import sqlalchemy as sa

from project.services.transfer import TransferService
from project.db import models
from project.types import UUID, Money, Currency
from project.auth import get_current_user

router = APIRouter()


class TransferCreateIn(BaseModel):
    from_account_id: UUID
    to_account_id: UUID
    amount: Money


class TransferOut(BaseModel):
    id: UUID

    to_account_id: UUID
    from_account_id: UUID

    amount: Money
    total_amount: Money

    created_at: datetime.datetime


class TransferSortBy(Enum):
    created_at = "created_at",
    desc_created_at = "-created_at",
    currency = "currency"
    

@router.get("/transfers", response_model=List[TransferOut])
async def get_transfer_list(
    account_id: Optional[UUID] = None,
    currency: Optional[Currency] = None,
    sort_by: Optional[TransferSortBy] = TransferSortBy.desc_created_at,
    current_user=Depends(get_current_user),
):
    query = models.Transfer.query.select_from(
        models.Transfer.join(
            models.Account,
            sa.or_(models.Transfer.from_account_id == models.Account.id,
                   models.Transfer.to_account_id == models.Account.id)
        )
    ).where(models.Account.customer_id == current_user.customer_id)

    if currency is not None:
        query = query.where(models.Account.currency == currency)

    if account_id is not None:
        query = query.where(models.Account.id == account_id)

    order_by_field = {
        TransferSortBy.created_at: models.Transfer.created_at,
        TransferSortBy.desc_created_at: models.Transfer.created_at.desc(),
        TransferSortBy.currency: models.Account.currency,
    }.get(cast(TransferSortBy, sort_by))
    query = query.order_by(order_by_field)

    transfers = await query.gino.fetch_all()
    return transfers


@router.post("/transfers", response_model=TransferOut, status_code=201)
async def create_transfer(
    transfer_in: TransferCreateIn,
    current_user=Depends(get_current_user),
):
    transfer_service = TransferService()

    try:
        transfer = await transfer_service.create(
            from_account_id=transfer_in.from_account_id,
            to_account_id=transfer_in.to_account_id,
            amount=transfer_in.amount,
            customer_id=current_user.customer_id,
        )
    except transfer_service.NotEnoughMoney:
        raise HTTPException(
            status_code=402,
            detail="Not enough money on account"
        )
    except transfer_service.AccountNotFound:
        raise HTTPException(
            status_code=404,
            detail="Account not found",
        )

    return transfer


@router.get("/transfers/{trans_id}", response_model=TransferOut)
async def get_transfer(
    trans_id: UUID,
    current_user=Depends(get_current_user),
):
    query = models.Transfer.query.select_from(
        models.Transfer.join(
            models.Account,
            sa.or_(models.Transfer.from_account_id == models.Account.id,
                   models.Transfer.to_account_id == models.Account.id)
        )
    ).where(
        models.Account.customer_id == current_user.customer_id,
        models.Transfer.id == trans_id,
    )

    transfer = await query.gino.first()
    if not transfer:
        raise HTTPException(status_code=404, detail="Transfer not found.")

    return transfer
