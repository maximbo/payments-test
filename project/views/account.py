import datetime
from typing import Optional, List

from fastapi import APIRouter, Depends, HTTPException
from pydantic import BaseModel

from project.services.account import AccountService
from project.types import Money, UUID, Currency
from project.auth import get_current_user
from project.db import models

router = APIRouter()


class AccountCreateIn(BaseModel):
    currency: Currency


class AccountOut(BaseModel):
    id: UUID
    customer_id: UUID

    currency: Currency
    balance: Money

    created_at: datetime.datetime
    updated_at: datetime.datetime


@router.get("/accounts", response_model=List[AccountOut])
async def get_account_list(
    currency: Optional[Currency] = None,
    current_user=Depends(get_current_user),
):
    query = models.Account.query.where(
        models.Account.customer_id == current_user.customer_id
    )

    if currency is not None:
        query = query.where(models.Account.currency == currency)
    print(query, current_user)

    return [row.to_dict() async for row in query.gino.iterate()]


@router.post("/accounts", response_model=AccountOut, status_code=201)
async def create_account(
    account_in: AccountCreateIn,
    current_user=Depends(get_current_user),
):
    account_service = AccountService()
    account = await account_service.create(
        customer_id=current_user.customer_id,
        currency=account_in.currency,
    )

    return account


@router.get("/accounts/{account_id}", response_model=AccountOut)
async def get_account_details(
    account_id: UUID,
    current_user=Depends(get_current_user),
):
    account = await models.Account.query.where(
        models.Account.id == account_id,
        models.Account.customer_id == current_user.customer_id,
    ).first()
    if account is None:
        raise HTTPException(status_code=404, detail="Account does not exist")

    return account
