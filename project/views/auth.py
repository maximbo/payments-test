from typing import Optional

from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm
from pydantic import BaseModel, validator

from project.types import UUID
from project.services.auth import AuthService, AccessToken

router = APIRouter()


class LoginIn(BaseModel):
    username: str
    password: str


class RegistrationIn(BaseModel):
    username: str
    password: str

    first_name: Optional[str] = ''
    middle_name: Optional[str] = ''
    last_name: Optional[str] = ''

    @validator("username")
    def not_empty_username(cls, v):
        v = v.strip()
        if not v:
            raise ValueError("Username must not be empty.")
        return v

    @validator("password")
    def not_empty_password(cls, v):
        v = v.strip()
        if not v:
            raise ValueError("Password must not be empty.")
        return v


class CustomerOut(BaseModel):
    id: UUID
    first_name: Optional[str] = ''
    middle_name: Optional[str] = ''
    last_name: Optional[str] = ''


class RegistrationOut(BaseModel):
    username: str
    token: AccessToken
    customer: CustomerOut


@router.post("/auth/token", response_model=AccessToken)
async def login_for_access_token(
    form_data: OAuth2PasswordRequestForm = Depends(),
):
    auth_service = AuthService()
    user = await auth_service.authenticate(
        username=form_data.username,
        password=form_data.password,
    )
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )

    return auth_service.create_access_token(user)


@router.post(
    "/auth/register",
    response_model=RegistrationOut,
    status_code=201,
)
async def register(data: RegistrationIn):
    auth_service = AuthService()

    if await auth_service.is_username_unavailable(data.username):
        raise HTTPException(
            status_code=409,
            detail="Username is already taken",
        )

    auth_user, customer = await auth_service.create_with_customer(
        username=data.username,
        password=data.password,
        first_name=data.first_name,
        middle_name=data.middle_name,
        last_name=data.last_name,
    )

    token_data = auth_service.create_access_token(auth_user)
    customer_data = CustomerOut(**customer.to_dict())

    response = RegistrationOut(
        token=token_data,
        username=auth_user.username,
        customer=customer_data,
    )
    return response
