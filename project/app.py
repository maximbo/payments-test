from fastapi import FastAPI

from project.db import db


def register_routers(app: FastAPI, prefix=str):
    from project.views import (
        auth,
        transfer,
        account,
    )

    app.include_router(account.router, prefix=prefix)
    app.include_router(transfer.router, prefix=prefix)
    app.include_router(auth.router, prefix=prefix)


def create_app() -> FastAPI:
    app = FastAPI()
    db.init_app(app)

    register_routers(app, prefix="/api")

    return app


app = create_app()
