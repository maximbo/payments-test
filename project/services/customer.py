from typing import Optional

from project.services import ServiceBase
from project.types import Currency, Money

from project.db import db, models


class CustomerService(ServiceBase):
    async def create(
        self,
        first_name: Optional[str] = '',
        middle_name: Optional[str] = '',
        last_name: Optional[str] = '',
    ) -> models.Customer:
        async with db.transaction():
            customer = await models.Customer.create(
                first_name=first_name,
                middle_name=middle_name,
                last_name=last_name,
            )

            # Создаём долларовый счёт и сразу начисляем туда 100 буказоидов
            await models.Account.create(
                customer_id=customer.id,
                currency=Currency.USD,
                balance=Money(100),
            )

            await models.Account.create(
                customer_id=customer.id,
                currency=Currency.RUB,
                balance=Money(0),
            )

            await models.Account.create(
                customer_id=customer.id,
                currency=Currency.EUR,
                balance=Money(0),
            )

            return customer
