from typing import Optional, Tuple
from datetime import datetime, timedelta

from passlib.context import CryptContext
from jose import JWTError, jwt

from pydantic import BaseModel

from project.types import UUID
from project.services import ServiceBase
from project.services.customer import CustomerService
from project.db import db, models
from project.config import (
    AUTH_ACCESS_TOKEN_EXPIRE_MINUTES,
    AUTH_HASH_ALGORITHM,
    SECRET_KEY,
)


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


class AccessToken(BaseModel):
    access_token: str
    token_type: str

    def __str__(self):
        return f"{self.token_type} {self.access_token}"

    @property
    def header(self):
        return {"Authorization": str(self)}


class AccessTokenData(BaseModel):
    username: str
    user_id: str
    customer_id: Optional[UUID]


class AuthService(ServiceBase):
    class UsernameNotAvailable(Exception):
        pass

    async def is_username_unavailable(
        self,
        username: str,
    ) -> bool:
        user_exists = await db.scalar(db.exists().where(
            models.AuthUser.username == username
        ).select())
        return user_exists

    async def create(
        self, *,
        username: str,
        password: str,
        customer_id: Optional[UUID] = None,
    ) -> models.AuthUser:
        hashed_password = get_password_hash(password)

        async with db.transaction():
            if await self.is_username_unavailable(username):
                raise self.UsernameNotAvailable

            return await models.AuthUser.create(
                username=username,
                hashed_password=hashed_password,
                customer_id=customer_id,
            )

    async def get_by_token(self, token: str) -> Optional[models.AuthUser]:
        decoded_token = self.decode_token(token)
        if not decoded_token:
            return None

        return await models.AuthUser.query.where(
            models.AuthUser.id == decoded_token.user_id
        ).gino.first()

    async def create_with_customer(
        self, *,
        username: str,
        password: str,
        first_name: Optional[str] = '',
        middle_name: Optional[str] = '',
        last_name: Optional[str] = '',
    ) -> Tuple[models.AuthUser, models.Customer]:
        async with db.transaction():
            customer = await CustomerService().create(
                first_name=first_name,
                middle_name=middle_name,
                last_name=last_name,
            )
            auth_user = await self.create(
                username=username,
                password=password,
                customer_id=customer.id,
            )
            return (auth_user, customer)

    async def authenticate(
        self, *,
        username: str,
        password: str,
    ) -> Optional[models.AuthUser]:
        user = await models.AuthUser.query.where(
            models.AuthUser.username == username
        ).gino.first()
        if not user:
            return None

        if not self.is_password_correct(password, user.hashed_password):
            return None

        return user

    def create_access_token(self, user: models.AuthUser) -> AccessToken:
        data = {
            "username": user.username,
            "user_id": str(user.id),
        }

        if user.customer_id is not None:
            data["customer_id"] = str(user.customer_id)

        token = create_jwt_token(data=data)
        return AccessToken(
            access_token=token,
            token_type="bearer",
        )

    def decode_token(self, token: str) -> Optional[AccessTokenData]:
        try:
            payload = jwt.decode(token, str(SECRET_KEY), algorithms=[
                AUTH_HASH_ALGORITHM,
            ])
            return AccessTokenData(**payload)
        except JWTError:
            return None

    def is_password_correct(
        self,
        password: str,
        hashed_password: str,
    ) -> bool:
        return pwd_context.verify(password, hashed_password)


def get_password_hash(password: str) -> str:
    return pwd_context.hash(password)


def create_jwt_token(
    data: dict,
    expires_delta: Optional[timedelta] = None
) -> str:
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + AUTH_ACCESS_TOKEN_EXPIRE_MINUTES
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(
        to_encode,
        str(SECRET_KEY),
        algorithm=AUTH_HASH_ALGORITHM,
    )
    return encoded_jwt
