from typing import List, Optional
import sqlalchemy as sa

from project.services import ServiceBase
from project.types import UUID, Money
from project.repos.takeoff import TakeoffRepo
from project.repos.currency_exchange import CurrencyExchangeRepo
from project.db import db, models


class TransferService(ServiceBase):

    class AccountNotFound(Exception):
        """Аккаунт не найден"""

    class SenderAccountNotFound(AccountNotFound):
        pass

    class ReceiverAccountNotFound(AccountNotFound):
        pass

    class NotEnoughMoney(Exception):
        """Недостаточно денег на счёте для перевода"""

    async def create(
        self, *,
        from_account_id: UUID,
        to_account_id: UUID,
        amount: Money,
        customer_id: UUID,
    ) -> models.Transfer:
        takeoff_repo = TakeoffRepo()
        currency_exchange_repo = CurrencyExchangeRepo()

        async with db.transaction():
            from_account = await models.Account.query.where(
                models.Account.id == from_account_id,
                models.Account.customer_id == customer_id,
            ).with_for_update().first()
            if from_account is None:
                raise self.AccountNotFound

            to_account = await models.Account.query.where(
                models.Account.id == to_account_id,
            ).with_for_update().first()
            if to_account is None:
                raise self.AccountNotFound

            total_amount = amount

            # Подстройка под курс обмена
            rate = currency_exchange_repo.get_rate(
                from_account.currency,
                to_account.currency,
            )
            total_amount = total_amount * rate

            # Подстройка под перевод между счетами
            rate = takeoff_repo.get_rate(from_account, to_account)
            total_amount = total_amount * rate

            if from_account.balance - total_amount < 0:
                raise self.NotEnoughMoney

            await models.Account.update.values({
                "balance": models.Account.balance - total_amount,
            }).where(models.Account.id == from_account_id)

            await models.Account.update.values({
                "balance": models.Account.balance + total_amount,
            }).where(models.Account.id == to_account_id)

            return await models.Transfer.create(
                from_account_id=from_account_id,
                to_account_id=to_account_id,
                amount=amount,
                total_amount=total_amount)
