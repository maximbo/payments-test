from project.services import ServiceBase
from project.types import UUID, Money, Currency

from project.db import models


class AccountService(ServiceBase):
    async def create(
        self, *,
        customer_id: UUID,
        balance: Money = Money(0),
        currency: Currency,
    ) -> models.Account:
        return await models.Account.create(
            customer_id=customer_id,
            balance=balance,
            currency=currency,
        )
