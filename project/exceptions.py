class DBException(Exception):
    pass


class NotFound(DBException):
    """Данные не найдены"""


class AccountException(Exception):
    pass


class NotEnoughMoney(AccountException):
    """Недостаточно денег на счёте"""
