import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID as DBUUID

from project.types import Currency
from project.db import db

DBMoney = sa.Numeric(10, 2)


class AuthUser(db.Model):
    __tablename__ = "auth_user"

    id = db.Column(DBUUID(as_uuid=False), primary_key=True,
                   server_default=sa.text("uuid_generate_v4()"))

    username = db.Column(db.Unicode(), unique=True, nullable=False)
    hashed_password = db.Column(db.Unicode(), nullable=False,
                                server_default="")

    customer_id = db.Column(DBUUID, db.ForeignKey('customer.id'), index=True)

    created_at = db.Column(sa.DateTime, nullable=False,
                           server_default=sa.func.now())
    updated_at = db.Column(sa.DateTime, nullable=False,
                           server_default=sa.func.now(),
                           server_onupdate=sa.func.now())

    def __str__(self):
        return self.username


class Customer(db.Model):
    __tablename__ = "customer"

    id = db.Column(DBUUID(as_uuid=False), primary_key=True,
                   server_default=sa.text("uuid_generate_v4()"))

    first_name = db.Column(db.Unicode, nullable=False, server_default="")
    middle_name = db.Column(db.Unicode, nullable=False, server_default="")
    last_name = db.Column(db.Unicode, nullable=False, server_default="")

    created_at = db.Column(sa.DateTime, nullable=False,
                           server_default=sa.func.now())
    updated_at = db.Column(sa.DateTime, nullable=False,
                           server_default=sa.func.now(),
                           server_onupdate=sa.func.now())

    def __str__(self):
        return " ".join((self.first_name, self.middle_name, self.last_name))


class Account(db.Model):
    __tablename__ = "account"

    id = db.Column(DBUUID(as_uuid=False), primary_key=True,
                   server_default=sa.text("uuid_generate_v4()"))
    balance = db.Column(DBMoney, nullable=False, server_default='0.0')
    currency = db.Column(sa.Enum(Currency), nullable=False)
    customer_id = db.Column(DBUUID, db.ForeignKey('customer.id'), index=True)

    created_at = db.Column(sa.DateTime, nullable=False,
                           server_default=sa.func.now())
    updated_at = db.Column(sa.DateTime, nullable=False,
                           server_default=sa.func.now(),
                           server_onupdate=sa.func.now())


class Transfer(db.Model):
    __tablename__ = "transfer"

    id = db.Column(DBUUID(as_uuid=False), primary_key=True,
                   server_default=sa.text("uuid_generate_v4()"))

    # Сумма перевода
    amount = db.Column(DBMoney, nullable=False)

    # Сумма перевода с учётом комиссий
    total_amount = db.Column(DBMoney, nullable=False)

    # Отправитель
    from_account_id = db.Column(DBUUID, db.ForeignKey('account.id'),
                                index=True, nullable=False)
    # Получатель
    to_account_id = db.Column(DBUUID, db.ForeignKey('account.id'),
                              index=True, nullable=False)

    created_at = db.Column(sa.DateTime, nullable=False,
                           server_default=sa.func.now())
