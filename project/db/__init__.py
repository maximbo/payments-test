from gino.ext.starlette import Gino

from project import config


db = Gino(
    dsn=str(config.DATABASE_URL),
    use_connection_for_request=not config.TESTING,
)
