from project.repos import BaseRepo

from project.types import Currency, Money


class CurrencyExchangeRepo(BaseRepo):
    """Курс обмена валют"""

    CURRENCY_RATES = {
        (Currency.RUB, Currency.RUB): Money(1),
        (Currency.EUR, Currency.EUR): Money(1),
        (Currency.USD, Currency.USD): Money(1),

        (Currency.USD, Currency.RUB): Money(75),
        (Currency.RUB, Currency.USD): Money('0.70'),

        (Currency.EUR, Currency.RUB): Money('80'),
        (Currency.RUB, Currency.EUR): Money('0.75'),

        (Currency.USD, Currency.EUR): Money('0.2'),
        (Currency.EUR, Currency.USD): Money('2'),
    }

    def get_rate(
        self,
        from_currency: Currency,
        to_currency: Currency,
    ) -> Money:
        return self.CURRENCY_RATES[(from_currency, to_currency)]
