from project.repos import BaseRepo
from project.types import Currency, Money

from project.db import models


class TakeoffRepo(BaseRepo):
    """Процент, взымаемый при переводе денег с одного счёта на другой"""

    # Процент при переводе между счетами одного клиента
    LOCAL_CONVERSION_RATE = Money(1)

    # Процент при переводе между счетами разных клиентов
    TAKEOFF_RATES = {
        (Currency.EUR, Currency.EUR): Money(1.1),
        (Currency.RUB, Currency.RUB): Money(1.1),
        (Currency.USD, Currency.USD): Money(1.1),

        (Currency.USD, Currency.USD): Money(1.1),
        (Currency.EUR, Currency.EUR): Money(1.1),

        (Currency.USD, Currency.RUB): Money(1.1),
        (Currency.RUB, Currency.USD): Money(1.1),

        (Currency.RUB, Currency.EUR): Money(1.1),
        (Currency.EUR, Currency.RUB): Money(1.1),

        (Currency.USD, Currency.EUR): Money(1.1),
        (Currency.EUR, Currency.USD): Money(1.1),
    }

    def get_rate(
        self,
        from_account: models.Account,
        to_account: models.Account,
    ) -> Money:
        if from_account.customer_id == to_account.customer_id:
            return self.LOCAL_CONVERSION_RATE

        query = (from_account.currency, to_account.currency)
        return self.TAKEOFF_RATES[query]
