from starlette.config import Config
from starlette.datastructures import Secret

config = Config(".env")

API_PREFIX = "/api"

TESTING = config("TESTING", cast=bool, default=False)
SECRET_KEY = config("SECRET_KEY", cast=Secret, default="CHANGE-ME-NOW")

AUTH_HASH_ALGORITHM = config("AUTH_HASH_ALGORITHM", cast=str, default="HS256")
AUTH_ACCESS_TOKEN_EXPIRE_MINUTES = config(
    "AUTH_ACCESS_TOKEN_EXPIRE_MINUTES", cast=int, default=30)

DB_USER = config("DB_USER", cast=str, default="postgres")
DB_PASS = config("DB_PASS", cast=Secret, default="postgres")
DB_HOST = config("DB_HOST", cast=str, default="db")
DB_PORT = config("DB_PORT", cast=str, default="5432")
DB_NAME = config("DB_NAME", cast=str)
DATABASE_URL = config(
  "DATABASE_URL",
  cast=str,
  default=f"postgresql://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
)

TEST_DB_USER = config("TEST_DB_USER", cast=str, default="postgres")
TEST_DB_PASS = config("TEST_DB_PASS", cast=Secret, default="postgres")
TEST_DB_HOST = config("TEST_DB_HOST", cast=str, default="db")
TEST_DB_PORT = config("TEST_DB_PORT", cast=str, default="5432")
TEST_DB_NAME = config("TEST_DB_NAME", cast=str)
TEST_DATABASE_URL = config(
  "TEST_DATABASE_URL",
  cast=str,
  default=f"postgresql://{TEST_DB_USER}:{TEST_DB_PASS}@{TEST_DB_HOST}:{TEST_DB_PORT}/{TEST_DB_NAME}"
)

if TESTING:
    DATABASE_URL = TEST_DATABASE_URL
